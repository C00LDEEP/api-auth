<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProductController;


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

//Route::apiResource('/product', ProductController::class)->middleware('auth:api');
Route::post('/product/store',[ProductController::class,'store'])->middleware('auth:api');
Route::put('/product/{product}',[ProductController::class,'update'])->middleware('auth:api');
Route::delete('/product/{product}',[ProductController::class,'destroy'])->middleware('auth:api');
Route::get('/product',[ProductController::class,'index'])->middleware('auth:api');